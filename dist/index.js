(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["vue-svgicon"] = factory();
	else
		root["vue-svgicon"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(1);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var Component = __webpack_require__(2)(
  /* script */
  __webpack_require__(3),
  /* template */
  __webpack_require__(4),
  /* styles */
  null,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/cult/Projects/vue-svgicon/src/component/svgicon.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] svgicon.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a8523cee", Component.options)
  } else {
    hotAPI.reload("data-v-a8523cee", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 2 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});


var icons = {};
var notLoadedIcons = [];
var defaultWidth = '';
var defaultHeight = '';

exports.default = {
  data: function data() {
    return {
      loaded: false
    };
  },

  props: {
    icon: String,
    name: String,
    width: {
      type: String,
      default: ''
    },
    height: {
      type: String,
      default: ''
    },
    scale: String,
    dir: String,
    fill: {
      type: Boolean,
      default: true
    },
    color: String
  },

  computed: {
    clazz: function clazz() {
      var clazz = 'svg-icon';

      if (this.fill) {
        clazz += ' svg-fill';
      }

      if (this.dir) {
        clazz += ' svg-' + this.dir;
      }

      return clazz;
    },
    iconName: function iconName() {
      return this.name || this.icon;
    },
    iconData: function iconData() {
      if (this.iconName && this.loaded) {
        return icons[this.iconName];
      }

      return null;
    },
    colors: function colors() {
      if (this.color) {
        return this.color.split(' ');
      }
      return '';
    },
    path: function path() {
      var _this = this;

      if (this.iconData) {
        var reg = /<(path|rect|circle|polygon|line|polyline|ellipse)\s/gi;

        if (this.colors && this.colors.length > 0) {
          var i = 0;

          return this.iconData.data.replace(reg, function (match) {
            var color = _this.colors[i++] || _this.colors[_this.colors.length - 1];
            var fill = _this.fill;

            if (color && color.indexOf('r-') === 0) {
              fill = !fill;
              color = color.split('r-')[1];
            }

            var style = fill ? 'fill' : 'stroke';
            var reverseStyle = fill ? 'stroke' : 'fill';
            return match + (style + '="' + color + '" ' + reverseStyle + '="none" ');
          });
        } else {
          return this.iconData.data;
        }
      } else {
        notLoadedIcons.push({
          name: this.iconName,
          component: this
        });
      }

      return '';
    },
    box: function box() {
      var width = this.width || 16;
      var height = this.width || 16;

      if (this.iconData) {
        if (this.iconData.viewBox) {
          return this.iconData.viewBox;
        }
        return '0 0 ' + this.iconData.width + ' ' + this.iconData.height;
      }

      return '0 0 ' + parseFloat(width) + ' ' + parseFloat(height);
    },
    style: function style() {
      var digitReg = /^\d+$/;
      var scale = Number(this.scale);
      var width = void 0;
      var height = void 0;

      if (!isNaN(scale) && this.iconData) {
        width = Number(this.iconData.width) * scale + 'px';
        height = Number(this.iconData.height) * scale + 'px';
      } else {
        width = digitReg.test(this.width) ? this.width + 'px' : this.width;
        height = digitReg.test(this.height) ? this.height + 'px' : this.height;
      }

      return {
        width: width || defaultWidth,
        height: height || defaultHeight
      };
    }
  },

  created: function created() {
    if (icons[this.iconName]) {
      this.loaded = true;
    }
  },
  install: function install(Vue) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var tagName = options.tagName || 'svgicon';

    options.defaultWidth && (defaultWidth = options.defaultWidth);
    options.defaultHeight && (defaultHeight = options.defaultHeight);

    Vue.component(tagName, this);
  },
  register: function register(data) {
    var _loop = function _loop(name) {
      if (!icons[name]) {
        icons[name] = data[name];
      }

      notLoadedIcons = notLoadedIcons.filter(function (v, ix) {
        if (v.name === name) {
          v.component.$set(v.component, 'loaded', true);
        }

        return v.name !== name;
      });
    };

    for (var name in data) {
      _loop(name);
    }
  },


  icons: icons
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('svg', {
    class: _vm.clazz,
    style: (_vm.style),
    attrs: {
      "version": "1.1",
      "viewBox": _vm.box
    },
    domProps: {
      "innerHTML": _vm._s(_vm.path)
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-a8523cee", module.exports)
  }
}

/***/ })
/******/ ]);
});
//# sourceMappingURL=index.js.map